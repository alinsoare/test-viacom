#!/bin/bash -e

BUILDTYPE=${1}

PREV_COMMITDATE=$(TZ=UTC git log --date='format-local:%Y%m%d' --pretty=format:"%ad" -n 2|tail -n1)
CURRDATE=$(TZ=UTC date +%Y%m%d)
MAJOR=$(grep platformVersion platform.properties|cut -d= -f2|cut -d. -f1)
GIT_VER_TMP=$(git for-each-ref --sort=-taggerdate --count=1 --format '%(tag)' refs/tags/${BUILDTYPE}Version*|cut -d- -f2)
GIT_VER=${GIT_VER_TMP:-"${MAJOR}.0.0"}
GIT_VER_MAJ=`echo $GIT_VER |cut -d. -f1`
GIT_VER_MIN=`echo $GIT_VER |cut -d. -f2`
GIT_VER_BUG=`echo $GIT_VER |cut -d. -f3`

echo -e "PREV_COMMITDATE = ${PREV_COMMITDATE}\nCURRDATE = ${CURRDATE}\nGIT_VER = ${GIT_VER}"

[[ -z "$MAJOR" ]] && exit 1 # exit if unset or empty

if [[ "$GIT_VER_MAJ" != "$MAJOR" && "$BUILDTYPE" != "beta" ]]
then
  # alpha only
  echo "Changing $BUILDTYPE MAJOR version"
  GIT_VER_MAJ=${MAJOR}
  GIT_VER_MIN="0"
  GIT_VER_BUG="0"
else
  # alpha/beta
  if [[ "$PREV_COMMITDATE" != "$CURRDATE" && GIT_VER != "${MAJOR}.0.0" ]]
  then
    echo "Changing $BUILDTYPE MINOR version"
    GIT_VER_MIN="$(($GIT_VER_MIN+1))"
    GIT_VER_BUG="0"
  else
    echo "Changing $BUILDTYPE BUGFIX version"
    GIT_VER_BUG="$(($GIT_VER_BUG+1))"
  fi
fi

VER="${GIT_VER_MAJ}.${GIT_VER_MIN}.${GIT_VER_BUG}"
git tag -a ${BUILDTYPE}Version-${VER} $GIT_COMMIT -m "set version ${BUILDTYPE} tag ${VER}" && git push origin ${BUILDTYPE}Version-${VER}
