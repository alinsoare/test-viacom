#!/bin/bash

git checkout master

MAJOR=`grep platformVersion platform.properties|cut -d= -f2|cut -d. -f1`
[[ "$MAJOR" =~ ^[0-9]+$ ]] && sed -i 's/platformVersion='${MAJOR}'\.0\.0/platformVersion='$((${MAJOR}+1))'\.0\.0/' platform.properties
git add platform.properties && git commit -m "platform version increased to $((${MAJOR}+1))" && git push origin master
