/**
 * Class that fetch/calculate next platform version number or unified version number, based on build type,
 * current build number and last build timestamp in milliseconds
 *
 * Usage for local development and checks:
 *
 * groovy platformVersioning.groovy <<buildType>> <<currentBetaMajorVersion>> <<lastBuildSemanticVersion>> <<lastBuildTimestampInMilliseconds>>
 * buildType can be: alpha, beta
 * currentBetaMajorVersion - current beta major version, for example 33
 * lastBuildSemanticVersion - last semantic version of build, for example 33.0.2
 * lastBuildTimestampInMilliseconds - last build's start timestamp, in milliseconds
 *
 * Usage from Jenkinsfile - import as a shared library and use platformVersioning.findNextVersion method
 */

@Grab(group = 'commons-lang', module = 'commons-lang', version = '2.6')
import org.apache.commons.lang.time.DateUtils

String buildType = (args.length >= 1) ? args[0] : "alpha"
String currentBetaVersion = (args.length >= 2) ? args[1] : ""
String lastBuildVersion = (args.length >= 3) ? args[2] : ""
lastBuildVersion = lastBuildVersion != "null" ? lastBuildVersion : null
long previousStartTime = (args.length >= 4) ? Long.parseLong(args[3]) : 0
println "CI Version number script input params :: buildType: ${buildType}, currentBetaVersion: ${currentBetaVersion}, lastVersion: ${lastBuildVersion}, previousStartTime: ${previousStartTime}"

String nextVersion = findNextVersion(buildType, currentBetaVersion, lastBuildVersion, previousStartTime)
println "Calculated next version: ${nextVersion}"

// from: https://gist.github.com/michaellihs/a6621376393821d6d206ccfc8dbf86ec
enum PatchLevel {
    MAJOR, MINOR, PATCH
}

class SemanticVersion implements Serializable {

    int major, minor, patch

    SemanticVersion(String version) {
        def versionParts = version.tokenize('.')
        if (versionParts.size != 3) {
            throw new IllegalArgumentException("Wrong version format - expected MAJOR.MINOR.PATCH - got ${version}")
        }
        this.major = versionParts[0].toInteger()
        this.minor = versionParts[1].toInteger()
        this.patch = versionParts[2].toInteger()
    }

    SemanticVersion(int major, int minor, int patch) {
        this.major = major
        this.minor = minor
        this.patch = patch
    }

    SemanticVersion bump(PatchLevel patchLevel) {
        switch (patchLevel) {
            case PatchLevel.MAJOR:
                return new SemanticVersion(major + 1, 0, 0)
            case PatchLevel.MINOR:
                return new SemanticVersion(major, minor + 1, 0)
            case PatchLevel.PATCH:
                return new SemanticVersion(major, minor, patch + 1)
        }
        throw new IllegalArgumentException("Wrong PatchLevel")
    }

    String toString() {
        return "${major}.${minor}.${patch}"
    }
}

String findNextVersion(String buildType, String currentBetaVersion, String lastBuildVersion, long previousStartTimeTimestamp) {
    return findNextVersion(buildType, currentBetaVersion, lastBuildVersion, new Date(previousStartTimeTimestamp))
}

/* Method to calculate next version based on current version and build time */
String findNextVersion(String buildType, String currentBetaVersion, String lastBuildVersion, Date previousStartTime) {
    int currentBetaMajor
    if (!currentBetaVersion.isInteger()) {
        throw IllegalArgumentException("currentBetaVersion is not an int")
    }
    currentBetaMajor = currentBetaVersion as Integer

    if (lastBuildVersion == null) {
        switch (buildType) {
            case "alpha":
                throw new IllegalArgumentException("Alpha build should have at least one previous version available")
            case "beta":
                return "${currentBetaMajor}.0.0"
            default:
                throw new IllegalArgumentException("Wrong build type: ${buildType}")
        }
    }

    // non null last build version
    SemanticVersion version = new SemanticVersion(lastBuildVersion)
    SemanticVersion nextVersion

    switch (buildType) {
        case "alpha":
            if (currentBetaMajor >= version.major) {
                nextVersion = new SemanticVersion(currentBetaMajor + 1, 0, 0)
            } else {
                nextVersion = bumpBasedOnDate(version, previousStartTime)
            }
            break
        case "beta":
            if (currentBetaMajor > version.major) {
                nextVersion = new SemanticVersion(currentBetaMajor, 0, 0)
            } else {
                nextVersion = bumpBasedOnDate(version, previousStartTime)
            }
            break
        default:
            throw new IllegalArgumentException("Wrong build type: ${buildType}")
    }
    
    return nextVersion.toString()
}

private SemanticVersion bumpBasedOnDate(SemanticVersion lastBuildVersion, Date previousStartTime) {
    Date currentDate = new Date()
    boolean isSameDay = DateUtils.isSameDay(currentDate, previousStartTime)

    SemanticVersion nextVersion
    if (isSameDay) {
        nextVersion = lastBuildVersion.bump(PatchLevel.PATCH)
    } else {
        nextVersion = lastBuildVersion.bump(PatchLevel.MINOR)
    }
    return nextVersion
}
