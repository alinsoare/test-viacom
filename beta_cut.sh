#!/bin/bash

export TZ="UTC"

DAYS=1
REFDATE=$(date -d "-${DAYS} days" +%Y%m%d)
MAJOR=`grep platformVersion platform.properties|cut -d= -f2|cut -d. -f1`
[[ -z "$MAJOR" ]] && exit 1 # exit if unset or empty

GIT_VER=`git for-each-ref --sort=-taggerdate --count=1 --format '%(tag)' refs/tags/betaVersion-\*.0.0|cut -d- -f2`
if [[ ! -z "$GIT_VER" ]]
then
    GIT_VER_MAJ=`echo $GIT_VER |cut -d. -f1`
    LAST_BETA_DATE=`git log -1 --date='format-local:%Y%m%d' --format=%cd betaVersion-${GIT_VER}`
    echo "Last beta cut date: $(date -d $LAST_BETA_DATE +%D)"
    [[ "$REFDATE" -lt "${LAST_BETA_DATE}" ]] && echo "Last beta was cutted less than ${DAYS} days ago! Abording.." && exit 1
fi

git rev-parse --verify --quiet origin/release/${MAJOR} >/dev/null 2>&1 && { echo "Beta release ${MAJOR} exists!" ;exit 1; } || git checkout -b release/${MAJOR}

#Need to make an empty commit on new release to trigger beta build
git commit --allow-empty -m "First commit on beta release ${MAJOR}"
git push origin release/${MAJOR}



            sh '''
            PREV_BETA="$(git ls-remote --refs 2>/dev/null|grep -Eo 'heads/release/[0-9]{1,4}'|sed 's/heads\\/release\\///' | sort -nr|head -1)"
            NEXT_BETA=$((${PREV_BETA}+1))
            PREV_BETA_DATE="$(git show --quiet --format='%cd' --date=format:'%Y%m%d' $(git merge-base origin/release/${PREV_BETA} master))"
            DIFFDATE=$(( $(date +%Y%m%d) - ${PREV_BETA_DATE} ))
            if [[ ${DIFFDATE} -lt 14 ]]; then
              [[ $FORCED ]] && git checkout -b release/${NEXT_BETA} && git push origin release/${NEXT_BETA}
            else
              git checkout -b release/${NEXT_BETA} && git push origin release/${NEXT_BETA}
            fi
            '''
