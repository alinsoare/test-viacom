@Grab('com.amazonaws:aws-java-sdk:1.9.3')
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.S3Object

/**
 * Class that fetch/calculate next platform version number or unified version number.
 * The derived value is written in platform.properties file which is then read by
 * gradle to set component version numbers.
 *
 * Following environment variables must be set in Jenkins configuration before
 * running this script
 *
 * ci_version_s3_bucket = S3 bucket that holds the version number file.
 * ci_version_s3_file = File that holds the version number.
 *
 * Usage:
 *
 * groovy CIVersionNumber <<buildType>>
 *
 * Outputs version number to file named platform.properties
 *
 */

String type = (args.length >= 1) ? args[0] : ""
println "CI Version number script input params :: build Type = ${type}"


CIVersionNumberUtil ciUtil = new CIVersionNumberUtil()
ciUtil.getNextVersionNumber(type)

class CIVersionNumberUtil {
     def getNextVersionNumber(String type) {
        String nextVersion

        Properties platformProperties = new Properties()
        File propertiesFile = new File('./jenkins/platform.properties')
        propertiesFile.withInputStream {
            platformProperties.load(it)
        }

         log("Find next ${type} version.")
         switch (type) {
            case "pr":
                //For PR builds, version number does not matter, but gradle throws error if
                // version number field is empty, so we get the commit number
                nextVersion = "git rev-parse --short HEAD".execute().text.trim()
                break;
            case "alpha":
            case "beta":
                //For alpha & beta builds, sprint number is obtained from platform.properties file.
                String platformVersion = platformProperties.getProperty("platformVersion")
                log("Value in platform.properties ${platformVersion}")
                String sprintNumber = platformVersion.tokenize('.').first()
                nextVersion = findNextVersion(type, sprintNumber)
                break
            case ["player", "bento", "tve", "shared"]:
                //For On-demand builds of these components, version is taken from the git tag.
                nextVersion = findVersionFromTag(type)
                break;
            default:
                log("No build type input parameter.")
        }
        log("Next ${type} version = ${nextVersion}")

        platformProperties.setProperty("platformVersion", nextVersion)
        propertiesFile.withPrintWriter {
            platformProperties.store(it, null)
        }
    }

    /**
     *
     * @param type : "alpha" , "beta"
     * @param nextSprintNumber : used only for beta builds, because there could be multiple beta branches
     * @return Next version number
     */
    String findNextVersion(String type, String nextSprintNumber) {
        String bucketName = getEnvPropertyValue("ci_version_s3_bucket")
        String keyName = getEnvPropertyValue("ci_version_s3_file")

        log("Get next version number for build type ${type} from bucket ${bucketName} and key ${keyName}")
        String nextVersion
        try {
            //Get AWS credentials from environment variables.
            AmazonS3 s3Client = new AmazonS3Client(new EnvironmentVariableCredentialsProvider());

            //Read the file and store it locally as temp file.A temp file is created because MD5 checksum
            //error was occurring for subsequent run's.
            log("Reading key ${keyName} from S3")
            S3Object s3object = s3Client.getObject(new GetObjectRequest(bucketName, keyName));
            InputStream inputStream = s3object.getObjectContent();
            Properties properties = new Properties();
            try {
                if (inputStream != null) {
                    properties.load(inputStream)
                }
            } finally {
                if (inputStream != null) {
                    inputStream.close()
                }
            }

            File propertiesFile = File.createTempFile("tmp_", "versions.properties")
            propertiesFile.deleteOnExit();
            properties.store(propertiesFile.newWriter(), null)

            //Compute the next version.
            nextVersion = computeNext(properties, type, nextSprintNumber)
            properties.store(propertiesFile.newWriter(), null)

            //Update the value in S3 bucket
            s3Client.putObject(new PutObjectRequest(bucketName, keyName, propertiesFile));
        } catch (Exception ex) {
            //Any exceptions, fail the build.
            throw new Exception("Exception occured from ci version script. ${ex.message}", ex)
        }
        return nextVersion;
    }

    /**
     * Returns the next version number for type (alpha/beta). In case correct type cannot be determined
     * returns default value as obtained from key "ci.version.default.number" or "0.0.0"
     *
     * @param properties
     * @param versionType : "alpha" , "beta"
     * @param nextSprintNumber : used only for beta builds, because there could be multiple beta branches
     * @return
     */
    String computeNext(Properties properties, String versionType, String nextSprintNumber) {
        log("Calculate next version from properties = ${properties} , versionType = ${versionType}")
        
        def currentDate = new Date()
        
        //Read existing (most recent) values
        def latestAlphaVersion = properties['latestAlphaVersion'] ?: "10.0.0"
        log("Alpha version ${latestAlphaVersion} read from S3")
        def (alphaSprint, alphaDay, alphaBuild) = latestAlphaVersion.split("\\.")

        def betaVersionPrefix = nextSprintNumber ?: alphaSprint
        def betaVersion = properties['latestBeta' + betaVersionPrefix + 'Version'] ?: latestAlphaVersion
        log("Beta version ${betaVersion} read from S3")
        def (betaSprint, betaDay, betaBuild) = betaVersion.split("\\.")

        def latestBetaCutDate = properties['latestBetaCutDate']

        if ("alpha".equalsIgnoreCase(versionType)) {
            def latestAlphaVersionDate = properties['latestAlphaVersionDate']
            log("Alpha start date ${latestBetaCutDate}")
            log("Alpha last run ${latestAlphaVersionDate}")

            if (alphaSprint != nextSprintNumber) { // alternate: (daysPassed(latestBetaCutDate, currentDate, 14))
                // Update alpha "sprint" number
                log("Alpha version - sprint change.")
                
                alphaSprint = alphaSprint.toInteger() + 1
                alphaDay = "0"
                alphaBuild = "0"
                properties['latestBetaCutDate'] = new Date().format('MM/dd/yyyy')
            } else if (daysPassed(latestAlphaVersionDate, currentDate, 1)) {
                // Update alpha "day" number every day
                log("Alpha version - day change.")
                
                alphaDay = alphaDay.toInteger() + 1
                alphaBuild = "0"
            } else { 
                // Update alpha "build" number upon every execution of this script on same day
                log("Alpha version - build change.")
                alphaBuild = alphaBuild.toInteger() + 1
            }

            properties['latestAlphaVersionDate'] = new Date().format('MM/dd/yyyy')
            properties['latestAlphaVersion'] = alphaSprint + "." + alphaDay + "." + alphaBuild

            return "${properties['latestAlphaVersion']}"
        } else if ("beta".equalsIgnoreCase(versionType)) {
            def latestBetaVersionDate = properties['latestBeta' + betaVersionPrefix + 'VersionDate']
            log("Beta last run ${latestBetaVersionDate}")
            if (daysPassed(latestBetaVersionDate, currentDate, 1)) {
                log("Beta day version change.")
                //Update beta "day" number very day
                betaDay = betaDay.toInteger() + 1
                betaBuild = "0"
            } else {
                log("Beta bug fix version change.")
                //Update beta "build" number upon every execution of this script on same day
                betaBuild = betaBuild.toInteger() + 1
            }
            properties['latestBeta' + betaVersionPrefix + 'VersionDate'] = new Date().format('MM/dd/yyyy')
            properties['latestBeta' + betaSprint + 'Version'] = betaSprint + "." + betaDay + "." + betaBuild

            return "${properties['latestBeta' + betaSprint + 'Version']}"
        }
        return null
    }


    // HELPER FUNCTIONS

    /*Method to find version from git Tag*/

    String findVersionFromTag(String type) {
        String tagName = "git describe".execute().text.trim()
        log("Tag name ${tagName}")
        List textTokenized = tagName.tokenize('/')
        if (textTokenized != null && textTokenized.contains(type)) {
            return textTokenized.last()
        }
    }

    /**
     * Finds out if "numberOfDays" have passed since "startDate".
     * Returns : true or false
     * @param startDate
     * @param numberOfDays
     * @return
     */
    boolean daysPassed(String startDate, Date endDate, int numberOfDays) {
        def start = startDate ? new Date().parse('MM/dd/yyyy', startDate) : endDate
        int diff = endDate.minus(start)
        if (diff >= 0 && diff < numberOfDays) {
            return false
        } else {
            return true
        }
    }

    /**
     * Method to read properties from CI environment variables.
     * @param propertyKey : Environment variable name
     * @return Environment variable value.
     */
    String getEnvPropertyValue(String propertyKey) {
        String propertyValue = System.getenv(propertyKey)
        if (!propertyValue) {
            throw new Exception("Property ${propertyKey} not found. Set this property as environment variable.")
        }
        return propertyValue
    }

    /**
     * Prints message to Jenkins console, in a nice readable format
     * @param string : Message to print in console.
     * @return nothing
     */
    def log(String string) {
        println("\033[36;1m\033[40;1m * [versionScript] ${string}\033[0m")
    }
}